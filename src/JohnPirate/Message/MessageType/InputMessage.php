<?php

namespace JohnPirate\Message\MessageType;


/**
 * Class InputMessage
 * @package JohnPirate\Message\MessageType
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 * @version 0.0.1
 */
class InputMessage
{

    const TEXT = 'text';
    const EMAIL = 'email';

    /**
     * @since 0.0.1
     *
     * @var array
     */
    protected $possible_types = [
        self::TEXT,
        self::EMAIL,
    ];

    /**
     * @since 0.0.1
     *
     * @var array
     */
    protected $default_options = [
        'type' => self::TEXT,
        'name' => null,
        'value' => null,
        'label' => null,
        'placeholder' => null,
        'id' => null,
    ];

    /**
     * @since 0.0.1
     *
     * @var string
     */
    public $type;

    /**
     * @since 0.0.1
     *
     * @var
     */
    public $name;

    /**
     * @since 0.0.1
     *
     * @var
     */
    public $value;

    /**
     * @since 0.0.1
     *
     * @var
     */
    public $label;

    /**
     * @since 0.0.1
     *
     * @var
     */
    public $placeholder;

    /**
     * @since 0.0.1
     *
     * @var
     */
    public $id;

    /**
     * InputMessage constructor.
     *
     * @param array $options
     */
    public function __construct (array $options = [])
    {
        $options = array_merge($this->default_options, $options);

        $this->type = in_array($options['type'], $this->possible_types)
            ? $options['type']
            : self::TEXT;
        $this->name = $options['name'];
        $this->value = $options['value'];
        $this->label = $options['label'];
        $this->placeholder = $options['placeholder'];
        $this->id = $options['id'];
    }

    /**
     * @since 0.0.1
     *
     * @return string
     */
    public function getId ()
    {
        return isset($this->id) ? $this->id : ('form' . ucfirst(strtolower($this->type)) . ucfirst($this->name));
    }
}