<?php

namespace JohnPirate\Message\MessageType;


/**
 * Class LineMessage
 * @package JohnPirate\Message\MessageType
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 * @version 0.0.1
 */
class LineMessage
{
    /**
     * @since 0.0.1
     *
     * @var string
     */
    public $text;

    /**
     * LineMessage constructor.
     *
     * @param string $text
     */
    public function __construct($text = '')
    {
        $this->text = $text;
    }
}