<?php

namespace JohnPirate\Message\MessageType;


/**
 * Class ButtonMessage
 * @package JohnPirate\Message\MessageType
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 * @version 0.0.1
 */
class ButtonMessage
{
    const SUBMIT = 'submit';
    const RESET = 'reset';
    const BUTTON = 'button';

    /**
     * @since 0.0.1
     *
     * @var
     */
    public $type;

    /**
     * @since 0.0.1
     *
     * @var
     */
    public $text;

    /**
     * @since 0.0.1
     *
     * @var array
     */
    protected $possible_types = [
        self::SUBMIT,
        self::RESET,
        self::BUTTON,
    ];

    /**
     * ButtonMessage constructor.
     *
     * @param string $type
     * @param string $text
     */
    public function __construct ($type = '', $text = '')
    {
        $this->type = in_array($type, $this->possible_types) ? $type : self::BUTTON;
        $this->text = $text;
    }
}