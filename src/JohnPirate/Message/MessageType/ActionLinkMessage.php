<?php

namespace JohnPirate\Message\MessageType;


/**
 * Class ActionLinkMessage
 * @package JohnPirate\Message\MessageType
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 * @version 0.0.1
 */
class ActionLinkMessage
{
    /**
     * @since 0.0.1
     *
     * @var string
     */
    public $text;

    /**
     * @since 0.0.1
     *
     * @var string
     */
    public $url;

    /**
     * ActionLinkMessage constructor.
     */
    public function __construct($text = '', $url = '')
    {
        $this->text = $text;
        $this->url = $url;
    }
}