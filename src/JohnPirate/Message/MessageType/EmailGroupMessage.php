<?php

namespace JohnPirate\Message\MessageType;

use JohnPirate\Message\MessageType\InputMessage;
use JohnPirate\Message\MessageType\ButtonMessage;


/**
 * Class EmailGroupMessage
 * @package JohnPirate\Message\MessageType
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 * @version 0.0.1
 */
class EmailGroupMessage
{
    /**
     * @since 0.0.1
     *
     * @var \JohnPirate\Message\MessageType\InputMessage
     */
    public $input;

    /**
     * @since 0.0.1
     *
     * @var \JohnPirate\Message\MessageType\ButtonMessage
     */
    public $button;

    /**
     * EmailGroupMessage constructor.
     *
     * @param array  $options_input
     * @param string $text_button
     */
    public function __construct(array $options_input = [], $text_button = '')
    {
        $this->input = new InputMessage($options_input);
        $this->button = new ButtonMessage(ButtonMessage::SUBMIT, $text_button);
    }
}