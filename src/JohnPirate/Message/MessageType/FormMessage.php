<?php

namespace JohnPirate\Message\MessageType;


/**
 * Class FormMessage
 * @package JohnPirate\Message\MessageType
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 * @version 0.0.1
 */
class FormMessage
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';
    const PATCH = 'PATCH';

    /**
     * @since 0.0.1
     *
     * @var array
     */
    protected $request_methods = [
        self::GET,
        self::POST,
        self::PUT,
        self::DELETE,
        self::PATCH,
    ];

    /**
     * @since 0.0.1
     *
     * @var string
     */
    public $method = 'GET';

    /**
     * @since 0.0.1
     *
     * @var null
     */
    public $action = null;

    /**
     * @since 0.0.1
     *
     * @var array
     */
    public $form_elements = [];

    /**
     * FormMessage constructor.
     */
    public function __construct ()
    {
    }

    /**
     * @since 0.0.1
     *
     * @param string $method
     *
     * @return $this
     */
    public function method($method = '')
    {
        $this->method = in_array($method, $this->request_methods) ? $method : self::GET;
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @return \JohnPirate\Message\MessageType\FormMessage
     */
    public function GET()
    {
        return $this->method(self::GET);
    }

    /**
     * @since 0.0.1
     *
     * @return \JohnPirate\Message\MessageType\FormMessage
     */
    public function POST()
    {
        return $this->method(self::POST);
    }

    /**
     * @since 0.0.1
     *
     * @return \JohnPirate\Message\MessageType\FormMessage
     */
    public function PUT()
    {
        return $this->method(self::PUT);
    }

    /**
     * @since 0.0.1
     *
     * @return \JohnPirate\Message\MessageType\FormMessage
     */
    public function DELETE()
    {
        return $this->method(self::DELETE);
    }

    /**
     * @since 0.0.1
     *
     * @return \JohnPirate\Message\MessageType\FormMessage
     */
    public function PATCH()
    {
        return $this->method(self::PATCH);
    }

    /**
     * @since 0.0.1
     *
     * @param string $action
     *
     * @return $this
     */
    public function action($action = '')
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @param string $text
     *
     * @return $this
     */
    public function line($text = '')
    {
        $this->form_elements[] = new LineMessage($text);
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @param string $text
     * @param string $url
     *
     * @return $this
     */
    public function actionLink($text = '', $url = '')
    {
        $this->elements[] = new ActionLinkMessage($text, $url);
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @param string $type
     * @param string $text
     *
     * @return $this
     */
    public function button($type = '', $text = '')
    {
        $this->form_elements[] = new ButtonMessage($type, $text);
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @param string $text
     *
     * @return \JohnPirate\Message\MessageType\FormMessage
     */
    public function submit($text = '')
    {
        return $this->button(ButtonMessage::SUBMIT, $text);
    }

    /**
     * @since 0.0.1
     *
     * @param array $options
     *
     * @return $this
     */
    public function input(array $options = [])
    {
        $this->form_elements[] = new InputMessage($options);
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @param array  $options_input
     * @param string $text_button
     *
     * @return $this
     */
    public function email(array $options_input = [], $text_button = '')
    {
        $this->form_elements[] = new EmailGroupMessage($options_input, $text_button);
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'method' => $this->method,
            'action' => $this->action,
            'elements' => $this->form_elements,
        ];
    }
}