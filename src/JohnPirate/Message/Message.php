<?php

namespace JohnPirate\Message;

use \JohnPirate\Message\MessageType\ActionLinkMessage;
use \JohnPirate\Message\MessageType\FormMessage;
use \JohnPirate\Message\MessageType\LineMessage;

/**
 * Class Message
 * @package JohnPirate\Message
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 * @version 0.0.1
 */
class Message
{
    /**
     * @since 0.0.1
     *
     * @var string
     */
    public $level = 'info';

    /**
     * @since 0.0.1
     *
     * @var string
     */
    public $subject = '';

    /**
     * @since 0.0.1
     *
     * @var string
     */
    public $lead = '';

    /**
     * @since 0.0.1
     *
     * @var array
     */
    public $elements = [];

    /**
     * Message constructor.
     */
    public function __construct()
    {
    }

    /**
     * @since 0.0.1
     *
     * @return $this
     */
    public function success()
    {
        $this->level = 'success';
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @return $this
     */
    public function warning()
    {
        $this->level = 'warning';
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @return $this
     */
    public function error()
    {
        $this->level = 'error';
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @param string $text
     *
     * @return $this
     */
    public function subject($text = '')
    {
        $this->subject = $text;
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @param string $text
     *
     * @return $this
     */
    public function lead($text = '')
    {
        $this->lead = $text;
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @param string $text
     *
     * @return $this
     */
    public function line($text = '')
    {
        $this->elements[] = new LineMessage($text);
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @param string $text
     * @param string $url
     *
     * @return $this
     */
    public function actionLink($text = '', $url = '')
    {
        $this->elements[] = new ActionLinkMessage($text, $url);
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @param callable $callback
     *
     * @return $this
     */
    public function form($method = '', $action = '', callable $callback)
    {
        $form = (new FormMessage)
            ->method($method)
            ->action($action);

        call_user_func_array($callback, array(&$form));

        $this->elements[] = $form;
        return $this;
    }

    /**
     * @since 0.0.1
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'level' => $this->level,
            'subject' => $this->subject,
            'lead' => $this->lead,
            'elements' => $this->elements,
        ];
    }
}