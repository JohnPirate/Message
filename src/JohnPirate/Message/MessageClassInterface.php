<?php

namespace JohnPirate\Message;

/**
 * Interface MessageClassInterface
 * @package JohnPirate\Message
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 * @version 0.0.1
 */
interface MessageClassInterface
{
    /**
     * @return \JohnPirate\Message\Message
     */
    public function init();
}