<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once __DIR__ . '/vendor/autoload.php';

use JohnPirate\Message\MessageType\ActionLinkMessage;
use JohnPirate\Message\MessageType\FormMessage;
use JohnPirate\Message\MessageType\LineMessage;


$message = (new \JohnPirate\Message\Message())
    ->success()
    ->subject('This is a subject')
    ->lead('This is a lead Text')
    ->line('Hello, I\'m a line text.')
    ->actionLink('Go to daily-five', 'https://www.daily-five.net')
    ->form('GET', '/', function($form) {

        $form->line('FORM TEXT');

        $form->input([
            'type' => 'text',
            'name' => 'email',
            'placeholder' => 'E-Mail Adresse',
            'label' => 'Gültige E-Mail Adresse eingeben',
        ]);

        $form->submit('Abschicken');

        $form->email([
            'type' => 'email',
            'name' => 'email',
            'placeholder' => 'E-Mail Adresse'
        ], 'Abschicken');

    })
    ->line('Text after the form');


var_dump($message->toArray());

extract($message->toArray());

require_once __DIR__ . '/templates/default.php';