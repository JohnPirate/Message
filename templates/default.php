<?php
use JohnPirate\Message\MessageType\ActionLinkMessage;
use JohnPirate\Message\MessageType\FormMessage;
use JohnPirate\Message\MessageType\LineMessage;
use JohnPirate\Message\MessageType\ButtonMessage;
use JohnPirate\Message\MessageType\InputMessage;
use JohnPirate\Message\MessageType\EmailGroupMessage;
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Message Template</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

<br>
<br>
<div class="container">


    <?php if (!empty($subject)): ?>
        <div class="row">
            <div class="col-sm-12">
                <h1 class="h3"><?= $subject ?></h1>
            </div>
        </div> <!-- // end: row -->
    <?php endif; ?>



    <?php if (!empty($lead)): ?>
        <div class="row">
            <div class="col-sm-12">
                <p class="lead"><?= $lead ?></p>
            </div>
        </div> <!-- // end: row -->
    <?php endif; ?>


    <?php foreach($elements as $element): ?>

        <div class="row">
            <div class="col-sm-12">

                <?php if ($element instanceof ActionLinkMessage): ?>
                    <div class="form-group">
                    <?php if ($level === 'success'): ?>
                        <a href="<?= $element->url ?>" class="btn btn-success"><?= $element->text ?></a>
                    <?php elseif ($level === 'warning'): ?>
                        <a href="<?= $element->url ?>" class="btn btn-warning"><?= $element->text ?></a>
                    <?php elseif ($level === 'error'): ?>
                        <a href="<?= $element->url ?>" class="btn btn-danger"><?= $element->text ?></a>
                    <?php else: ?>
                        <a href="<?= $element->url ?>" class="btn btn-primary"><?= $element->text ?></a>
                    <?php endif; ?>
                    </div>
                <?php elseif ($element instanceof LineMessage): ?>
                    <p><?= $element->text ?></p>
                <?php elseif ($element instanceof FormMessage): ?>
                    <form method="<?= $element->method ?>" action="<?= $element->action ?>">

                        <?php foreach($element->form_elements as $form_element): ?>

                            <div class="row">
                                <div class="col-sm-12">

                                    <?php if ($form_element instanceof ActionLinkMessage): ?>
                                        <div class="form-group">
                                        <?php if ($level === 'success'): ?>
                                            <a href="<?= $form_element->url ?>" class="btn btn-success"><?= $form_element->text ?></a>
                                        <?php elseif ($level === 'warning'): ?>
                                            <a href="<?= $form_element->url ?>" class="btn btn-warning"><?= $form_element->text ?></a>
                                        <?php elseif ($level === 'error'): ?>
                                            <a href="<?= $form_element->url ?>" class="btn btn-danger"><?= $form_element->text ?></a>
                                        <?php else: ?>
                                            <a href="<?= $form_element->url ?>" class="btn btn-primary"><?= $form_element->text ?></a>
                                        <?php endif; ?>
                                        </div>
                                    <?php elseif ($form_element instanceof LineMessage): ?>
                                        <p><?= $form_element->text ?></p>
                                    <?php elseif ($form_element instanceof ButtonMessage): ?>
                                        <div class="form-group">
                                            <button type="<?= $form_element->type ?>" class="btn btn-primary"><?= $form_element->text ?></button>
                                        </div>
                                    <?php elseif ($form_element instanceof InputMessage): ?>
                                        <div class="form-group">
                                            <?php if (isset($form_element->label)): ?>
                                                <label for="<?= $form_element->getId() ?>">
                                                    <?= $form_element->label ?>
                                                </label>
                                            <?php endif; ?>
                                            <input
                                                type="<?= $form_element->type ?>"
                                                name="<?= $form_element->name ?>"
                                                class="form-control"
                                                id="<?= $form_element->getId() ?>"
                                                placeholder="<?= $form_element->placeholder ?>"
                                                value="<?= $form_element->value ?>"
                                            >
                                        </div>
                                    <?php elseif ($form_element instanceof EmailGroupMessage): ?>

                                        <div class="row">
                                            <div class="col-sm-12 col-md-8">
                                                <div class="form-group">
                                                    <?php if (isset($form_element->input->label)): ?>
                                                        <label for="<?= $form_element->input->getId() ?>">
                                                            <?= $form_element->input->label ?>
                                                        </label>
                                                    <?php endif; ?>
                                                    <input
                                                        type="<?= $form_element->input->type ?>"
                                                        name="<?= $form_element->input->name ?>"
                                                        class="form-control"
                                                        id="<?= $form_element->input->getId() ?>"
                                                        placeholder="<?= $form_element->input->placeholder ?>"
                                                        value="<?= $form_element->input->value ?>"
                                                    >
                                                </div>
<!--                                                <div class="visible-sm visible-xs"><br></div>-->
                                            </div>
                                            <div class="col-sm-12 col-md-4">
                                                <div class="form-group">
                                                    <button class="btn btn-primary btn-block" type="<?= $form_element->button->type ?>">
                                                        <?= $form_element->button->text ?>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>


                                    <?php endif; ?>

                                </div>
                            </div>

                        <?php endforeach; ?>

                    </form>
                <?php endif; ?>

            </div>
        </div> <!-- // end: row -->

    <?php endforeach; ?>


</div> <!-- // end: container -->


</body>
</html>
